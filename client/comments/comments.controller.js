var myApp = angular.module('app');

myApp.controller('CommentsController', ['$scope', 'CommentsService','$stateParams', function($scope, CommentsService, $stateParams) {
  var query;
  
  if($stateParams.threads){
    query = CommentsService.getThreads(); 
  }
  else {
    query = CommentsService.getComments();
  }
  
  query.then(
      function(response){
        $scope.comments = response.data;
      },function(error){
        console.log('Error is ', JSON.stringify(error))
      })
}]);