(function() {
    'use strict';

    var client_id = '954498966175-pvnpk8f3isiebbl12gjg3gef7979ulob.apps.googleusercontent.com';
    var scope = "profile email openid";
    var redirect_uri = 'https://angular-asw.herokuapp.com';
    var response_type = "token";
    var fullUrl = "https://accounts.google.com/o/oauth2/auth?scope=" + scope + "&client_id=" + client_id + "&redirect_uri=" + redirect_uri + "&response_type=" + response_type;

    angular
        .module('app')
        .constant("Config", {
            baseUrlC9: "https://hacker-news-asw-ror-marcosperezrubio.c9users.io/",
            baseUrl: "https://hacker-news-asw-dcmm.herokuapp.com/",
            newsUrl: "api/v1/news/",
            submissionsUrl: "api/v1/submissions/",
            usersUrl: 'api/v1/users/',
            commentsUrl: 'api/v1/comments/',
            threadsUrl: 'api/v1/threads/',
            googleAPIUrl: "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=",
            authUrl: fullUrl
        })
})()