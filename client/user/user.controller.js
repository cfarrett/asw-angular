var myApp = angular.module('app');

myApp.controller('UserController', ['$scope', 'UsersService', '$stateParams', 'AuthService', function($scope, UsersService, $stateParams, AuthService) {
    $scope.canupdate = false;
    UsersService.getUserById($stateParams.id).then(
        function(response){
        $scope.user = response.data;
            var loggedUser = AuthService.getCurrentUser();
            if (loggedUser) {
                if(loggedUser.id == $stateParams.id) {
                    $scope.canupdate = true;
                    $scope.user['api-key'] = loggedUser['api-key'];
                }
            }
    },function(error){
        console.log('Error is ', JSON.stringify(error));
    });   
}]);

