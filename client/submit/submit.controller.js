var myApp = angular.module('app');

myApp.controller('SubmitController', ['$scope', 'SubmissionsService', function($scope, SubmissionsService) {

  $scope.errors = {
    title: '',
    url: '',
    message: ''
  };


  $scope.submit = function submit() {
    if (!$scope.title && !$scope.url && !$scope.message) {
      $scope.errors.title = 'Please provide a title';
      $scope.errors.url = 'Please provide a url or a message';
      $scope.errors.message = 'Please provide a url or a message';
    }
    else if (!$scope.title && !$scope.url && $scope.message) {
      $scope.errors.title = 'Please provide a title';
      $scope.errors.url = '';
      $scope.errors.message = '';
    }
    else if (!$scope.title && $scope.url && !$scope.message) {
      $scope.errors.title = 'Please provide a title';
      if (!isValidUrl($scope.url))
        $scope.errors.url = 'Please provide a valid url';
      $scope.errors.message = '';
    }
    else if (!$scope.title && $scope.url && $scope.message) {
      $scope.errors.title = 'Please provide a title';
      $scope.errors.url = 'Please provide a url or a message, not both';
      $scope.errors.message = 'Please provide a url or a message, not both';
    }
    else if ($scope.title && !$scope.url && !$scope.message) {
      $scope.errors.title = '';
      $scope.errors.url = 'Please provide a url or a message';
      $scope.errors.message = 'Please provide a url or a message';
    }
    else if ($scope.title && !$scope.url && $scope.message) {
      $scope.errors.title = '';
      $scope.errors.url = '';
      $scope.errors.message = '';
      SubmissionsService.postSubmission($scope.title, $scope.url, $scope.message);
    }
    else if ($scope.title && $scope.url && !$scope.message) {
      $scope.errors.title = '';
      $scope.errors.message = '';
      if (!isValidUrl($scope.url))
        $scope.errors.url = 'Please provide a valid url';
      else {
        SubmissionsService.postSubmission($scope.title, $scope.url, $scope.message);
      }
    }
    else if ($scope.title && $scope.url && $scope.message) {
      $scope.errors.title = '';
      $scope.errors.url = 'Please provide a url or a message, not both';
      $scope.errors.message = 'Please provide a url or a message, not both';
    }
  };

  var isValidUrl = function ValidURL(str) {
    var pattern = new RegExp("(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)");
    if (!pattern.test(str)) {
      return false;
    }
    else {
      return true;
    }
  };

}]);