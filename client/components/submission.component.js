var myApp = angular.module('app');

function submissionSingleController($state, $window, SubmissionsService, AuthService) {
  var ctrl = this;
  ctrl.sub = ctrl.item;


  if (ctrl.show) {
    ctrl.numbercomments = ctrl.sub.comments.length;
    ctrl.sub.comments.forEach(function(comment) {
      if(comment.replies) {
        ctrl.numbercomments += comment.replies.length;
      }
    })
  }


  ctrl.goToSubmission = function() {
    if (ctrl.sub.message) {
      $state.go('submission', {
        id: ctrl.sub.id
      });
    }
    else {
      $window.open(ctrl.sub.url, '_blank');
    }
  };

  ctrl.goToDiscuss = function() {
    $state.go('submission', {
      id: ctrl.sub.id
    });
  }

  ctrl.goToProfile = function() {
    $state.go('profile', {
      id: ctrl.sub.user_id
    });
  };

}

myApp
  .component('submissionComponent', {
    templateUrl: 'components/submission.single.html',
    bindings: {
      item: '=',
      position: '=',
      show: '='
    },
    controller: submissionSingleController
  });