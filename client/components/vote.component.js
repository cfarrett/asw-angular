var myApp = angular.module('app');

function voteComponentController(SubmissionsService, CommentsService, AuthService) {

    var ctrl = this;

    ctrl.currentUser = AuthService.getCurrentUser();

    if (ctrl.currentUser) ctrl.isFromUser = ctrl.currentUser.id == ctrl.item.user_id;

    if (!ctrl.item.hasBeenUpvoted) ctrl.item.hasBeenUpvoted = 0;

    ctrl.vote = function() {
        var toVote = ctrl.submission ? SubmissionsService.voteSubmission(ctrl.item.id) : CommentsService.voteComment(ctrl.item.id)

        toVote.then(
            function(ok) {
                if (ctrl.item.hasBeenUpvoted) {
                    --ctrl.item.points;
                }
                else {
                    ++ctrl.item.points;
                }
                ctrl.item.hasBeenUpvoted = !ctrl.item.hasBeenUpvoted
            },
            function(error) {
                console.log('Error is ', JSON.stringify(error));
            })
    }
}

myApp
    .component('vote', {
        templateUrl: 'components/vote.component.html',
        bindings: {
            item: '=',
            submission: '='
        },
        controller: voteComponentController
    });