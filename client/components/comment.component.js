var myApp = angular.module('app');

function commentSingleController($state, $window, SubmissionsService, CommentsService, AuthService){
  var ctrl = this;
  ctrl.com = ctrl.item;
  ctrl.replies = ctrl.com.replies;
  ctrl.isAuth = AuthService.isAuth();
  
  ctrl.goToSubmission = function(){
    $state.go('submission',{id:ctrl.com.submission_id});
  };

  ctrl.goToComment = function(){
    $state.go('comment',{id:ctrl.com.id});
  };
  
  ctrl.goToParent = function(){
    if (ctrl.com.parent_id) {
        $state.go('comment',{id:ctrl.com.parent_id});
    } else {
        $state.go('submission',{id:ctrl.com.submission_id})
    }
  };
  
  ctrl.goToProfile = function(){
    $state.go('profile',{id:ctrl.com.user_id});
  };
  
    ctrl.writeReply = function() {
    ctrl.error = '';
    if (ctrl.replyText) {
      if (ctrl.replyText.length >= 5) {
        CommentsService.postReply(ctrl.com.id, ctrl.replyText)
          .then(
            function(response) {
              $window.location.reload();
            },
            function(error) {
            })
      }
      else {
        ctrl.error = "Please make sure that your comment length is >= 5 characters."
      }
    }
    else {
      ctrl.error = "Please make sure that your comment length is >= 5 characters."
    }
  };
  
  if(!ctrl.show && !ctrl.under){
    SubmissionsService.getSubmissionById(ctrl.com.submission_id).then(
      function(response){
        ctrl.on = response.data.title;
      },function(error){
        console.log('Error is ', JSON.stringify(error));
    });
  }    
}
  

myApp
.component('commentComponent', {
  templateUrl: 'components/comment.single.html',
    bindings: {
    item: '=',
    show: '=',
    under: '='
  },
  controller: commentSingleController
});