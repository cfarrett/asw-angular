var myApp = angular.module('app');

function userSingleController($state, $window, UsersService){
  var ctrl = this;
  ctrl.user = ctrl.item;

  ctrl.updateAbout = function(){
    var putAbout = {};
    putAbout.about= ctrl.user.about;
    putAbout.id = ctrl.user.id;
    putAbout.api_key = ctrl.user['api-key'];
    UsersService.putAbout(putAbout).then(
      function(response){
        ctrl.message = "Your about has been succesfully updated";
      },function(error){
        console.log('Error is ', JSON.stringify(error));
    });
  };
}

myApp
.component('userComponent', {
  templateUrl: 'components/user.single.html',
    bindings: {
    item: '=',
    canupdate: '=',
    message: '=?bind'
  },
  controller: userSingleController
});