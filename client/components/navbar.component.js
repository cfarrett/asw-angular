angular
    .module('app')
    .directive('navbarComponent', ['AuthService','$location', function(AuthService, $location) {
        return {
            restrict: 'E',
            templateUrl: 'components/navbar.component.html',
            controller: function() {
                var that = this;

                that.currentUser = AuthService.getCurrentUser();
                
                that.login = function() {
                    AuthService.login($location.absUrl());
                }

                that.logout = function() {
                    AuthService.logout();
                    location.reload();
                }
                
            },
            controllerAs: 'ctrl'
        }
    }]);
