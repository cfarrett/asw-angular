var myApp = angular.module('app');

function replySingleController($state, $window, SubmissionsService, CommentsService){
  var ctrl = this;
  ctrl.reply = ctrl.item;
  ctrl.goToSubmission = function(){
    $state.go('submission',{id:ctrl.subid});
  };

  ctrl.goToComment = function(){
    $state.go('comment',{id:ctrl.reply.id});
  };

  ctrl.goToParent = function(){
    $state.go('comment',{id:ctrl.reply.parent_id});
  };
  
  ctrl.goToProfile = function(){
    $state.go('profile',{id:ctrl.reply.user_id});
  };
  
  if(ctrl.fullshow){
    SubmissionsService.getSubmissionById(ctrl.subid).then(
      function(response){
        ctrl.on = response.data.title;
      },function(error){
        console.log('Error is ', JSON.stringify(error));
    });
  }    
}
  

myApp
.component('replyComponent', {
  templateUrl: 'components/reply.single.html',
    bindings: {
    item: '=',
    fullshow: '=',
    subid: '='
  },
  controller: replySingleController
});