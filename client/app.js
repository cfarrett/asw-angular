
angular.module('app', ['ui.bootstrap','ui.router', 'ngLodash' ,'angular-clipboard'])
.config(function($stateProvider, $locationProvider, $urlRouterProvider) {


  var onlyLoggedIn = function($q, $window, AuthService) {
    var deferred = $q.defer();
    if (AuthService.isAuth()) {
      deferred.resolve();
    } else {
      deferred.reject();
      $window.location = "/"
      $window.location.load();
    }
    return deferred.promise;
  };

  // Now set up the states
  $stateProvider
    .state('submissions', {
      url: "/",
      templateUrl: "submissions/submissions.html",
      controller: "SubmissionsController"
    })
    .state('asks', {
      url: "/asks/",
      templateUrl: "asks/asks.html",
      controller: "AsksController"
    })
    .state('news', {
      url: "/news/",
      templateUrl: "news/news.html",
      controller: "NewsController"
    })
    .state('comments', {
      url: "/comments/",
      templateUrl: "comments/comments.html",
      controller: "CommentsController"
    })
    .state('threads', {
      url: "/threads/",
      params: {
        threads: true,
      },
      templateUrl: "comments/comments.html",
      controller: "CommentsController",
      resolve: {loggedIn:onlyLoggedIn}
    })
    .state('comment', {
      url: "/comments/:id",
      params: {
        threads: false,
      },
      templateUrl: "comment/comment.html",
      controller: "CommentController"
    })
    .state('submission', {
      url: "/submission/:id",
      templateUrl: "submission/submission.html",
      controller: "SubmissionController"
    })
    .state('login', {
      url: "/access_token=:accessToken",
      templateUrl: "oauth/waiting.html",
      controller: "LoginCallbackCtrl"
    })
    .state('profile', {
      url: "/users/:id",  
      templateUrl: "user/user.html",
      controller: "UserController"
    })
    .state('submit', {
      url: "/submit/",
      templateUrl: "submit/submit.html",
      controller: "SubmitController",
      resolve: {loggedIn:onlyLoggedIn}
    });

    $urlRouterProvider.otherwise('/');
    
    $locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});
});
