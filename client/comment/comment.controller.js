var myApp = angular.module('app');

myApp.controller('CommentController', ['$scope', 'CommentsService', '$stateParams',  function($scope,  CommentsService, $stateParams) {
  CommentsService.getCommentById($stateParams.id).then(
      function(response){
        $scope.com = response.data;
      },function(error){
        console.log('Error is ', JSON.stringify(error));
      });
}]);