var myApp = angular.module('app');

myApp.controller('LoginCallbackCtrl', function ($state, $scope, AuthService, $location, $window, $timeout) {
    var completed = false;
    
    var hash = $location.path().substr(1);
    var splitted = hash.split('&');
    var params = {};
    
    for (var i = 0; i < splitted.length; i++) {
        var param  = splitted[i].split('=');
        var key    = param[0];
        var value  = param[1];
        params[key] = value;
    }
    
    AuthService.createUser(params)
    .then(
      function(response){

        $timeout(function(){
          var prev = $window.localStorage.getItem('previousUrl');
          $window.location = prev  || '/'
          $window.location.load();
        }, 2150);


  },
      function(error){
        console.log('Error is ', JSON.stringify(error));
      }
      );
  });
