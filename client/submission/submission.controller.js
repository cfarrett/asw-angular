var myApp = angular.module('app');

myApp.controller('SubmissionController', ['$scope', 'SubmissionsService', '$stateParams', '$window', 'AuthService', function($scope, SubmissionsService, $stateParams, $window, AuthService) {


  $scope.isAuth = AuthService.isAuth();

  SubmissionsService.getSubmissionById($stateParams.id).then(
    function(response) {
      $scope.sub = response.data;
      $scope.comments = response.data.comments;
    },
    function(error) {
      console.log('Error is ', JSON.stringify(error));
    });


  $scope.makeComment = function() {
    $scope.error = '';
    if ($scope.commentText) {
      if ($scope.commentText.length >= 5) {
        SubmissionsService.replySubmission($scope.sub.id, $scope.commentText)
          .then(
            function(response) {
              $scope.commentText = '';
              $scope.comments.push(response.data);
            },
            function(error) {})
      }
      else {
        $scope.error = "Please make sure that your comment length is >= 5 characters."
      }
    }
    else {
      $scope.error = "Please make sure that your comment length is >= 5 characters."
    }
  }
}]);