var myApp = angular.module('app');

myApp.controller('NewsController', ['$scope', 'NewsService', function($scope, NewsService) {
  NewsService.getNews().then(
      function(response){
          $scope.news = response.data;
      },function(error){
          console.log('Error is ', JSON.stringify(error))
      })
}]);