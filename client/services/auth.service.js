angular.module("app")
    .factory('AuthService', function($http, Config, $q, UsersService, $window) {

        var baseURL = 'https://hacker-news-asw-dcmm.herokuapp.com';
        var submissionsURL = '/api/v1/asks';

        var factory = {};

        var loggedUser = null;


        factory.createUser = function(params) {
            var deferred = $q.defer();
            UsersService.getUserInfoByToken(params['access_token']).then(
                function(response) {
                    var paramsToPost = {};
                    paramsToPost.name = response.data.name;
                    paramsToPost.oauth_token = params['access_token'];
                    paramsToPost.uid = response.data.id;
                    paramsToPost.picture = response.data.picture;


                    var dateAux = new Date();
                    paramsToPost.oauth_expire_at = new Date(dateAux.getTime() + params['expires_in'] * 1000);

                    var d = new Date(paramsToPost.oauth_expire_at);
                    var dataChachi = d.getFullYear().toString() + '-' + ('0' + (d.getMonth() + 1)).slice(-2).toString() + '-' + ('0' + d.getDate()).slice(-2).toString() + ' ' + ('0' + d.getHours()).slice(-2).toString() + ':' + ('0' + d.getMinutes()).slice(-2).toString() + ':' + ('0' + d.getSeconds()).slice(-2).toString();
                    paramsToPost.oauth_expires_at = dataChachi;

                    UsersService.postUser(paramsToPost)
                        .then(
                            function(data) {
                                var userAux = data.data;
                                userAux.uid = paramsToPost.uid;
                                loggedUser = userAux;
                                $window.localStorage.setItem('user',JSON.stringify(loggedUser));
                                deferred.resolve();
                            },
                            function(error) {
                                console.log('Error is ', JSON.stringify(error))
                                deferred.reject();
                            });
                },
                function(error) {
                    console.log('Error is ', JSON.stringify(error));
                })
            return deferred.promise;
        };

        factory.login = function(previousUrl) {
            $window.localStorage.setItem('previousUrl',previousUrl);
            window.location.replace(Config.authUrl);
        };

        factory.logout = function() {
            $window.localStorage.removeItem('user');
        };

        factory.isAuth = function() {
            return Boolean(factory.getCurrentUser());
        };

        factory.getCurrentUser = function() {
            return JSON.parse($window.localStorage.getItem('user'));
        };

        return factory;
    });