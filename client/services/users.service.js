angular.module("app")
.factory('UsersService', function($http, $q, Config) {
   
   var baseURL = Config.baseUrl;
   var usersURL = Config.usersUrl;
   var currentUser;
   
   var factory = {};
   
   factory.getUsers = function() {
      return $http.get(baseURL + usersURL);
   };

   factory.getUserById = function(id) {
      return $http.get(baseURL + usersURL + id);
   };
   
   factory.getUserInfoByToken = function(token){
      return $http.get(Config.googleAPIUrl + token);
   };
   
   factory.setCurrentUser = function(user){
      currentUser = user;
   };
   
   factory.getCurrentUser = function(){
      return currentUser;
   };
   
   factory.postUser = function(paramsToPost) {
      return $http.post(baseURL + usersURL, paramsToPost)
   };
   
      factory.putAbout = function(aboutParams) {
         return $http({
            method: 'PUT',
            url: baseURL + usersURL +aboutParams.id,
            headers: {
               'id' : aboutParams.id,
               'api-key' : aboutParams.api_key
            },
            data: {'about': aboutParams.about}
      })
   };
  
   return factory;
}); 