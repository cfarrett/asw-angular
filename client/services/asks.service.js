angular.module("app")
.factory('AsksService', function($http, Config) {
   
   var baseURL = Config.baseUrl;
   var submissionsURL= '/api/v1/asks';
   
   var factory = {};
   
   factory.getAsks = function() {
      return $http.get(baseURL + submissionsURL);
   };
  
   return factory;
}); 