angular.module("app")
.factory('NewsService', function($http, Config) {
   var baseURL = Config.baseUrl;
   var newsURL= Config.newsUrl;
   
   var factory = {};
   
   factory.getNews = function() {
      return $http.get(baseURL + newsURL);
   };

   return factory;
}); 