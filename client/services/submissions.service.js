angular.module("app")
   .factory('SubmissionsService', function($http, Config, AuthService, $state) {
      var baseURL = Config.baseUrl;
      var submissionsURL = Config.submissionsUrl;

      var factory = {};

      function headerObject(url, method) {
         var currentUser = AuthService.getCurrentUser();

         var obj = {
            url,
            method
         }

         if (currentUser) {
            obj.headers = {
               "id": currentUser.id,
               "api-key": currentUser['api-key']
            }
         }

         return obj;
      }

      factory.getSubmissions = function() {
         var url = baseURL + submissionsURL
         return $http(headerObject(url, 'GET'))
      };

      factory.getSubmissionById = function(id) {
         return $http(headerObject(baseURL + submissionsURL + id,'GET'));
      };

      factory.voteSubmission = function(id) {
         var url = baseURL + submissionsURL + id + "/vote";
         return $http(headerObject(url, 'POST'));
      };

      factory.replySubmission = function(submissionId, message) {
         return $http({
            url: baseURL + submissionsURL + submissionId + '/reply',
            method: 'POST',
            headers: {
               'id': AuthService.getCurrentUser().id,
               'api-key': AuthService.getCurrentUser()['api-key']
            },
            data: {
               message
            }
         })
      }

      factory.postSubmission = function(title, url, message) {
         return $http({
            url: baseURL + "api/v1" + (url ? "/news" : "/asks"),
            method: 'POST',
            headers: {
               'id': AuthService.getCurrentUser().id,
               'api-key': AuthService.getCurrentUser()['api-key']
            },
            data: {
               'title': title,
               'url': url,
               'message': message
            }
         }).then(
            function(response) {
               $state.go('submission', {
                  id: response.data.id
               });
            },
            function(error) {
               console.log('Error is ', JSON.stringify(error));
            });
      }

      return factory;
   });