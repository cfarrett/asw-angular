angular.module("app")
   .factory('CommentsService', function($http, Config, AuthService) {
      var baseURL = Config.baseUrl;
      var commentsURL = Config.commentsUrl;
      var threadsURL = Config.threadsUrl;

      var factory = {};





      function headerObject(url, method) {
         var currentUser = AuthService.getCurrentUser();

         var obj = {
            url,
            method
         }

         if (currentUser) {
            obj.headers = {
               "id": currentUser.id,
               "api-key": currentUser['api-key']
            }
         }
         return obj;
      }






      factory.getComments = function() {
         return $http(headerObject(baseURL + commentsURL, 'GET'));
      };

      factory.getCommentById = function(id) {
         return $http(headerObject(baseURL + commentsURL + id, 'GET'));
      };

      factory.getThreads = function() {
         var loggedUser = AuthService.getCurrentUser();
         return $http({
            method: 'GET',
            url: baseURL + threadsURL,
            headers: {
               'id': loggedUser.id,
               'api-key': loggedUser['api-key']
            }
         })
      }

      factory.postReply = function(commentId, message) {
         var loggedUser = AuthService.getCurrentUser();
         return $http({
            method: 'POST',
            url: baseURL + commentsURL + commentId + "/reply",
            headers: {
               'id': loggedUser.id,
               'api-key': loggedUser['api-key']
            },
            data: {
               'message': message
            }
         })
      }


      factory.voteComment = function(id) {
         var url = baseURL + commentsURL + id + "/vote";
         return $http(headerObject(url, 'POST'));
      };

      return factory;
   });