var myApp = angular.module('app');

myApp.controller('AsksController', ['$scope', 'AsksService', function($scope, AsksService) {
  AsksService.getAsks().then(
      function(response){
        $scope.asks = response.data;
      },function(error){
        console.log('Error is ', JSON.stringify(error))
      })
}]);