var myApp = angular.module('app');

myApp.controller('SubmissionsController', ['$scope', 'SubmissionsService', function($scope, SubmissionsService) {
  SubmissionsService.getSubmissions().then(
      function(response){
        $scope.submissions = response.data.news.concat(response.data.asks);
      },function(error){
        console.log('Error is ', JSON.stringify(error))
      })
}]);