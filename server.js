//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
var http = require('http');
var path = require('path');
require('newrelic');


var express = require('express');

var app = express();
var server 
= http.createServer(app);


//opcion 1
//app.use('/bower_components', express.static(__dirname + '/bower_components'));
//app.use('/client', express.static(__dirname + '/client'));

//opcion 2
app.use(express.static(__dirname + '/client'));

app.get('/*',function(req,res){
    res.sendFile(__dirname + '/client/index.html');
})

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
});
